import React, { Component } from 'react';
import SearchResult from './components/SearchResult';
import Footer from './components/Footer';
import './App.scss';

class App extends Component {
  render() {
    return (
      <React.Fragment>
     <section className="company-heading intro-type" id="parallax-one">
  <div className="container">
    <div className="row product-title-info">
      <div className="col-md-12">
        <h1>Discover Space Missions</h1>
                <a className="ct-btn-scroll ct-js-btn-scroll" href="#section2">
                  <span>&nbsp;</span></a>
      </div>
    </div>
  </div>
  <div className="parallax" id="parallax-cta"></div>
</section>
<div className="main">
          <section id="section2">
            <div className="container">
              <SearchResult />
</div>
  
  </section>
        </div>
        <Footer />
   </React.Fragment>
    );
  }
}

export default App;
