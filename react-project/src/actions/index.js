import _ from 'lodash';
import jsonPlaceholder from '../apis';
import {FETCH_LIST, FETCH_LAUNCHPAD,  SEARCH_APPLY_FILTER   } from './actionTypes'; 

export const fetchList = () => async dispatch => {
        const response = await jsonPlaceholder.get('/launches');
        dispatch({
            type: FETCH_LIST,
            payload: response.data
        })
};

export const fetchLaunchPads = _.memoize(function() {
   return async function (dispatch) {
        const response = await jsonPlaceholder.get(`/launchpads`);
        dispatch({
            type: FETCH_LAUNCHPAD,
            payload: response.data
        });
    };
});

export const searchApplyfilter = (allData) => async dispatch => {
    const response = await jsonPlaceholder.get('/launches');
    dispatch({
        type: SEARCH_APPLY_FILTER,
        payload: response.data
    })
};