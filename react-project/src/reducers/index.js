import { combineReducers } from 'redux';
import fetchListReducer from './fetchListReducer';
import fetchLaunchPadReducer from './fetchLaunchPadReducer';
import searchApplyFilterReducer from './searchApplyReducer';

export default combineReducers({
    fetchAllItems: fetchListReducer,
    fetchLaunchPads: fetchLaunchPadReducer,
    searchApplyFilter: searchApplyFilterReducer
});