export default (state = [], action) => {
    switch (action.type) {
        case "FETCH_LAUNCHPAD":
            return action.payload;
        default:
            return state;
    }
}