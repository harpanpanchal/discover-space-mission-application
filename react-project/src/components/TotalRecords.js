import React from 'react';

const TotalRecords = (props) => {
    
    const totalCount = () =>  {
        if (props.totalCount <= 0) {
            return 'No Mission Found !!!'
        }
        else if (props.totalCount === 1) {
            return 'Showing 1 Mission'
        }
        else {
            return `Showing ${props.totalCount} Missions`
        }
    
    }
    return (<div className="row totalRecords">
        <div className="col-md-12 text-center searchTxt"><strong>{totalCount()}</strong></div>
    </div>)
};

export default TotalRecords;