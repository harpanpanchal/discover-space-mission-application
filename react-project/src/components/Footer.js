import React from 'react';

const Footer = () => {
    return (<footer className="text-muted">
        <div className="container">
            <div className="row">
                <div className="col-md-6 col-xs-12">
                <p>Copyright &copy; 2018 Space Savvy</p>
                </div>
                <div className="col-md-6 col-xs-12">
                <p>
                <a href="#section2">Back to top</a>
            </p>
                </div>
            </div>
           
        </div>
    </footer>)
};

export default Footer;