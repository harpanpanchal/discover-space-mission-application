import React from 'react';
const RenderStructure = (props) => {
    return(props.fetchAllItems.map(item => {
        return (<div className="row" key={item.flight_number}>
            <div className="col-md-2 text-center"><img src={item.links.mission_patch} alt="" /></div>
            <div className="col-md-8">
                <h2 className="title">{item.rocket.rocket_name} - {item.payloads[0].payload_id}
                    {(item.launch_success === false || item.land_success === false) ? <React.Fragment><span> - </span><span className="text-danger">Failed Mission</span></React.Fragment> : ''}</h2>
                <h3 className="description">Launched on <strong>{props.convertToDate(item.launch_date_local)}</strong> at <strong>{props.convertToTime(item.launch_date_local)}</strong> from <strong>{props.renderSingleLaunchPad(item.rocket.rocket_name)}</strong></h3>
                <div className="taggedBtns">
                    {(item.links.mission_patch) ? (<a href={item.links.mission_patch} className='btn btn-outline-secondary' target='_blank' rel="noopener noreferrer" title='Opens in a new window'>Mission Patch</a>) : ''}
    
                    {(item.links.article_link) ? (<a href={item.links.article_link} className='btn btn-outline-secondary' target='_blank' rel="noopener noreferrer" title='Opens in a new window'>Article</a>) : ''}
    
                    {(item.links.video_link) ? (<a href={item.links.video_link} className='btn btn-outline-secondary' target='_blank' rel="noopener noreferrer" title='Opens in a new window'>Watch Video</a>) : ''}

                    {(item.links.presskit) ? (<a href={item.links.presskit} className='btn btn-outline-secondary' target='_blank' rel="noopener noreferrer" title='Opens in a new window'>Press Kit</a>) : ''}

                    {(item.links.reddit_campaign) ? (<a href={item.links.reddit_campaign} className='btn btn-outline-secondary' target='_blank' rel="noopener noreferrer" title='Opens in a new window'>Reddit Campaign</a>) : ''}
                    {(item.links.reddit_launch) ? (<a href={item.links.reddit_launch} className='btn btn-outline-secondary' target='_blank' rel="noopener noreferrer" title='Opens in a new window'>Reddit Launch</a>) : ''}

                    {(item.links.reddit_media) ? (<a href={item.links.reddit_media} className='btn btn-outline-secondary' target='_blank' rel="noopener noreferrer" title='Opens in a new window'>Reddit Media</a>) : ''}
                </div>
            </div>
            <div className="col-md-2 text-center"><h4>#{item.flight_number}</h4>
                <p className="flightLabel text-center">Flight Number</p></div>
        </div>)
    }))
};

export default RenderStructure;