import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchList, fetchLaunchPads, searchApplyfilter} from '../actions';
import FormHeaderControls from './FormHeaderControls';
import TotalRecords from './TotalRecords';
import RenderStructure from './RenderStructure';
import _ from "lodash";
class SearchResult extends Component {
    constructor(props) {
        super(props)
        this.renderFormControls = this.renderFormControls.bind(this)
        this.searchApply = this.searchApply.bind(this)
        this.state = {
            term: '',
            launchPad: '',
            minYear: '',
            maxYear: '',
            searchApply: false
        };
  }
componentDidMount() {
        this.props.fetchList();
        this.props.fetchLaunchPads();
    }
    onInputChange = event => this.setState({ term: event.target.value });   
    onChangeLaunchPad = event => {
        (event.target.value === 'Any') ? this.setState({launchPad: ''}) : this.setState({launchPad: event.target.value})
 }
onChangeMinYear = event => {
        (event.target.value === 'Any') ? this.setState({minYear: ''}) : this.setState({minYear: event.target.value});
        }
onChangeMaxYear = event => {
        (event.target.value === 'Any') ? this.setState({maxYear: ''}) : this.setState({maxYear: event.target.value});
        }
onClickHandler = () => {
        this.setState({searchApply: true})
        this.props.searchApplyfilter();
}
onFormSubmit = event => event.preventDefault();
    renderLaunchPadName() {
            if (!this.props.fetchAllLaunchPads || this.props.fetchAllLaunchPads.length === 0) { 
            return("Loading")
        }
    return this.props.fetchAllLaunchPads.map(item => <option key={item.id}>{item.full_name}</option>)
}
      renderMinMaxYear() {
    if (this.props.fetchAllItems) {
    let uniques =  [];
    this.props.fetchAllItems.map((item, index) => {
    const dt = new Date(item.launch_date_local).getFullYear();
    uniques[index] = dt;
      return uniques;
    });
    
    for (let i = 0; i < uniques.length; i += 1) {
      let value = uniques[i];
      let found = false;
      for (let j = 0; j < uniques.length; j += 1) {
        let test = uniques[j];
        if (value === test) {
          found = true;
          break;
        }
      }
      if (!found) {
        uniques.push(value);
      }
    }
    return _.uniq(uniques).map(newarr => {
      return (<option key={newarr} value={newarr}>{newarr}</option>)
    })
      }
    }

    convertToDate = (fullDate) => new Date(fullDate).toLocaleDateString();
    convertToTime = (fullTime) => new Date(fullTime).toLocaleTimeString();
    renderSingleLaunchPad = (launchPad) =>  {
        if (!this.props.fetchAllLaunchPads || this.props.fetchAllLaunchPads.length === 0) { 
            return(<TotalRecords totalCount={0}/>)
        }
        else {
            const finalResult = this.props.fetchAllLaunchPads.filter(item => item.vehicles_launched === launchPad.toLowerCase());
            return finalResult[0].full_name
    }
    }
   
searchApply() {
if ((this.state.term === '') && (this.state.launchPad === '') && (this.state.minYear === '') && (this.state.maxYear === '')) {
            return (
                <React.Fragment>
                <TotalRecords totalCount={this.props.fetchAllItems.length}  />
                    <RenderStructure fetchAllItems={this.props.fetchAllItems} convertToDate={(fullDate) => this.convertToDate(fullDate)} convertToTime={(fullTime) => this.convertToTime(fullTime)} renderSingleLaunchPad={(launchPad) => this.renderSingleLaunchPad(launchPad)} />
                    </React.Fragment>)
            }
        else {
            let newQuery = ''; 
            newQuery = this.props.fetchAllItems.filter(item => {
            let finalQuery = '';    
            let searchByLaunchPadID;
            if (this.state.term !== '') {
            finalQuery = ((item.flight_number == this.state.term) || (item.rocket.rocket_name == this.state.term) || (item.payloads[0].payload_id == this.state.term));
            }
            if (this.state.launchPad !== '')
            {
                if (this.props.fetchAllLaunchPads) {
                    searchByLaunchPadID = this.props.fetchAllLaunchPads.filter(item => ((item.full_name === this.state.launchPad)));  
                    if (finalQuery !== '') {
                        finalQuery = finalQuery && item.launch_site.site_id == searchByLaunchPadID[0].id;
            }
                    else {
                        finalQuery = item.launch_site.site_id == searchByLaunchPadID[0].id
                    }
                }
            }
            if (this.state.minYear !== '')
                {
                if (finalQuery !== '') {
                finalQuery = finalQuery  && (new Date(item.launch_date_local).getFullYear() >= this.state.minYear)
                }
                else {
                    finalQuery = (new Date(item.launch_date_local).getFullYear() >= this.state.minYear)
                }
}
                    
            if(this.state.maxYear !== '') {      
                 if (finalQuery !== '') { 
                    finalQuery = finalQuery && (new Date(item.launch_date_local).getFullYear() <= this.state.maxYear)
                }
                else {
                    finalQuery = (new Date(item.launch_date_local).getFullYear() <= this.state.maxYear)
                 }
                }  
                return finalQuery            
            })
            if (newQuery.length === 0) {
                return(<TotalRecords totalCount={0}/>)
            }
            return (
                <React.Fragment>
                    <TotalRecords totalCount={newQuery.length}/>
                    <RenderStructure fetchAllItems={newQuery} convertToDate={(fullDate) => this.convertToDate(fullDate)} convertToTime={(fullTime) => this.convertToTime(fullTime)} renderSingleLaunchPad={(launchPad) => this.renderSingleLaunchPad(launchPad)} />
                </React.Fragment>   
                )
}
}
    displayFinalList() {
        if (this.state.searchApply) {
            return (this.searchApply())
        }
        else {
            return (<React.Fragment>
                <TotalRecords totalCount={this.props.fetchAllItems.length}  />
                <RenderStructure fetchAllItems={this.props.fetchAllItems} convertToDate={(fullDate) => this.convertToDate(fullDate)} convertToTime={(fullTime) => this.convertToTime(fullTime)} renderSingleLaunchPad={(launchPad) => this.renderSingleLaunchPad(launchPad)} />
            </React.Fragment>)
        }
    }
renderFormControls() {
        return (
            <React.Fragment>
                <div className="row formControls">
                <form onSubmit={this.onFormSubmit}>  
                    <div className="col-md-10">
                        <div className="form-row">
                            <div className="form-group col-md-3">
                                <label htmlFor="inputKeywords">Keywords</label>
<input type="text" placeholder='eg Falcon' className="form-control" id="inputKeywords" onChange={this.onInputChange} value={this.state.term} />
                            </div>
                            <div className="form-group col-md-5">
                                <label htmlFor="inputLaunchpad">Launch Pad</label>

                                <select id="inputLaunchpad" className="form-control" onChange={this.onChangeLaunchPad} value={this.state.launchPad}>
                                    <option>Any</option>
                                    {this.renderLaunchPadName()}
      
                                </select>
                            </div>
                            <div className="form-group col-md-2">
                                <label htmlFor="inputLaunchpad">Min Year</label>
                                    <select id="inputLaunchpad" className="form-control" onChange={this.onChangeMinYear} value={this.state.minYear}>
                                    <option>Any</option>
                                    {this.renderMinMaxYear()}
                                </select>
                            </div>
                          <div className="form-group col-md-2">
                                <label htmlFor="inputLaunchpad">Max Year</label>
                                <select id="inputLaunchpad" className="form-control" onChange={this.onChangeMaxYear} value={this.state.maxYear}>
                                    <option defaultValue>Any</option>
                                    {this.renderMinMaxYear()}
                                </select>
                            </div>
                          </div>
                   </div>
                    <div className="col-md-2">
        <div className="form-group col-md-12">
<button type="submit" className="btn applyBtn" onClick={this.onClickHandler}>Apply</button>    </div>
</div>
               </form>
                </div>
               
                {
            this.displayFinalList()
            }
</React.Fragment>)
    }
    render() {
        return (
            <FormHeaderControls renderFormControls={this.renderFormControls} />
)
    }
}
const mapStateToProps = state => {
    return {
        fetchAllItems: state.fetchAllItems,
        fetchAllLaunchPads: state.fetchLaunchPads,
        searchApplyFilter: state.searchApplyFilter
    }
};
export default connect(mapStateToProps, { fetchList, fetchLaunchPads, searchApplyfilter })(SearchResult);