# Welcome to the Discover Space Mission Application

## Introduction

This application provides data (API) search feature using filters by using React / Redux / Node.js.

### Solution Technologies

**React**

https://reactjs.org/

1. Change into the `react-project` folder and run `npm install`.
1. Start the project via `npm run start`.
1. You can then view the project by visiting `localhost:3000` in your browser.
1. Open the `react-project` in your editor to make changes

### API Server

This solution will require interaction with a local API server that provides data.

To start the server, use a separate terminal window with the following:

1. Change into the `server` directory and run `npm install`
1. Start the server via `npm run start`

The following 2 endpoints will now be available.

| Endpoint                         | Description                     |
| -------------------------------- | ------------------------------- |
| http://localhost:8001/launches   | returns an array of launch data |
| http://localhost:8001/launchpads | returns launchpads              |

### Stopping the projects and server

If at anytime you want to stop the project servers. Press keys `control + c` in the terminal.

#### Other technical specifications

1. Sass - CSS Preprocessor
2. Axios - Middleware
3. Responsiveness
